FROM centos/httpd

MAINTAINER Jose Castro Leon <jose.castro.leon@cern.ch>

RUN yum install epel-release mod_wsgi git -y && \
    yum install python2-pip -y && \
    yum clean all

WORKDIR /var/www/yaqluator

RUN cd /var/www && \
    git clone https://gitlab.cern.ch/cloud-infrastructure/yaqluator.git yaqluator -b master

RUN pip install -r /var/www/yaqluator/requirements.txt && \
    rm -f /etc/httpd/conf.d/*.conf

COPY yaqluator.conf /etc/httpd/conf.d/
